///<reference path='../defs/mongodb.ts' />

"use strict";
import conf = module('../conf');
import mongodb = module('mongodb');

var _db;
var pastes;

export function getPaste(id: string, cb) {
	if (! id) return cb(new Error("no id provided"));

	pastes.find({
		_id: new mongodb.ObjectID(id)
	}, function (err, cursor) {
		if(err) {
			return cb(err, null);
		}
		cursor.nextObject(function (err, doc) {
			if(err) {
				return cb(err, null);
			}
			cb(null, doc);
		});
	});
}

export function pasteByCookie(cookie: string, cb) {
	if (! cookie) return cb(new Error("no cookie provided"));

	pastes.find({
		cookie: cookie
	}, function (err, cursor) {
		if (err) return cb(err, null);
		return cursor.toArray(function (err, results) {
			cb(err, results);
		});
	});
}

export function addPaste(body, cb) {
	if (! body) return cb(new Error("null body provided"));

	pastes.insert({
		data: body.data,
		date: new Date(),
		cookie: body.cookie,
		parent: body.parent
	}, {}, function (err, objects) {
		cb(err, objects);
	});
}

export function editPaste(body, cb) {
	if (! body) return cb(new Error("null body provided"));

	pastes.findAndModify({
		_id: new mongodb.ObjectID(body.id),
		cookie: body.cookie
	}, [ [ '_id', 'asc' ] ], {
		data: body.data,
		date: new Date(),
		cookie: body.cookie,
		parent: body.parent
	}, {
		new: true
	}, function (err, objects) {
		cb(err, objects);
	});
}

export function init() {
    mongodb.connect(conf.connString, {
        'db': {
            'safe': true
        },
        'server': {
            'debug': true,
            'autoreconnect': true,
            'poolSize': 1
        }
    }, function (err, db) {
        if(err) {
            console.error(err);
            return err;
        }
        pastes = new mongodb.Collection(db, 'pastes');
    });
}

export function close() {
    _db.close();
}
