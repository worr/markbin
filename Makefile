.PHONY: build test clean

libdir := lib
modelsdir := models
websocketsdir := websockets

objects := conf.js $(modelsdir)/pastes.js $(websocketsdir)/api.js app.js

%.js: %.ts
	tsc $<

build: $(objects)

test: build
	@npm install .
	@./node_modules/.bin/nodeunit test

clean:
	-rm $(objects)
