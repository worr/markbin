///<reference path='node.d.ts' />

declare module "mongodb" {
   export class Server {
       constructor(host: string, port: number, opts?: any, moreopts?: any);
   }
   export class Db {
       constructor(databaseName: string, serverConfig: Server, options: any);
       public open(callback: ()=>void);
       public collection(name: string, callback: (err: any, collection: Collection) => void);    
   }
   export class ObjectID {
       constructor(s: string);
   }
   export class Collection {
	   constructor(client: any, s: string);
	   find(query: any): MongoCursor;
	   find(query: any, callback?: (err: any, result: any) => void): MongoCursor;
	   find(query: any, select: any, callback?: (err: any, result: any) => void): MongoCursor;
	   findOne(query: any, callback: (err: any, result: any) => void): void;
	   findAndModify(query: any, sort: any, updates: any, options: any, callback: (err: any, result: any) => void): MongoCursor;
	   update(query: any, updates: any, callback: (err: any, result: any) => void): void;
	   insert(query: any, options: any, callback?: (err: any, result: any) => void): void;
   }

   export function connect(url: string, options?: any, cb?: Function): any;
}


interface MongoDb {
   
}

interface MongoCursor {
   toArray(callback: (err: any, results: any[]) => void);
   nextObject(callback: (err: any, doc: any) => void);
}
