///<reference path='node.d.ts' />

declare module "node-uuid" {
	export function v1(options?: Uuidv1Options, buffer?: Array, offset?: Number): string;
	export function v4(options?: Uuidv4Options, buffer?: Array, offset?: Number): string;
}

interface Uuidv1Options {
	node: Array;
	clockseq: Number;
	msecs: any;
	nsecs: Number;
}

interface Uuidv4Options {
	random: Array;
	rng: () => Array;
}
