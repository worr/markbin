///<reference path='node.d.ts' />

declare module "http-proxy" {
	import http = module('http');
	import net = module('net');
	export function createServer(port?: any, host?: string, options?: any, handler?: (req: http.ServerRequest, res: http.ServerResponse, proxy: any) => any): any;
	export function buffer(object: any): any;
	export function getMaxSockets(): number;
	export function setMaxSockets(value: number): any;
	export function stack(middlewares: any[], proxy: NodeProxy): any;

	interface NodeProxy {
		proxyRequest(req: http.ServerRequest, res: http.ServerResponse, buffer?: any): any;
		proxyWebSocketRequest(req: http.ServerRequest, socket: net.NodeSocket, head: string, buffer?: any): any;
	}

	interface NodeProxyTable {
	}

	declare var HttpProxy: {
		new (options: any): NodeProxy;
		close(): any;
	}

	declare var ProxyTable: {
		new (options: any): NodeProxyTable;
		setRoutes(router: any): any;
		getProxyLocation(req: http.ServerRequest): any;
		close(): any;
	}

	declare var RoutingProxy: {
		new (options: any): NodeProxy;
		add(options: any): any;
		remove(options: any): any;
	}
}
