"use strict"
///<reference path='defs/node.d.ts' />
///<reference path='defs/socket.io.d.ts' />
///<reference path='defs/node-http-proxy.d.ts' />

import fs = module("fs");
import io = module("socket.io");
import http = module("http");

var app = http.createServer(handler);
app.listen(3000);

function handler(req, res) {
	var filePath = req.url;
	if (filePath == '/')
		filePath = '/public/index.html';

	fs.readFile(__dirname + filePath, function (err, data) {
		if (err) {
			res.writeHead(500);
			return res.end('Error loading index.html');
		}
		res.writeHead(200);
		res.end(data);
	});
}

// Start websockets server
var sio = io.listen(app);
import api = module("websockets/api");
import pastes = module("models/pastes");

pastes.init();
api.run(sio);
