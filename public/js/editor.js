"use strict"
var editor;
var firstrun = true;

function loadEditor() {
	$('#editor').show();
	$('.hide_editing').hide();
	$('.show_editing').show();

	if (firstrun) {
		editor = $('#editor');
		editor = ace.edit("editor");
		editor.setTheme("ace/theme/crimson_editor");
		editor.getSession().setMode("ace/mode/markdown");

		firstrun = false;
	}
}

function unloadEditor() {
	$('#editor').hide();
	$('.hide_editing').show();
	$('.show_editing').hide();
}
