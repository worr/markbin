"use strict"
///<reference path='../defs/node.d.ts' />
///<reference path='../defs/express.d.ts' />
///<reference path='../defs/marked.d.ts' />

import express = module("express");
import pastes = module("../models/pastes");
import marked = module("marked");

function broadcastPaste(socket: any, paste: any) {
	socket.broadcast.emit('recv', { id: paste._id, raw_data: paste.data, date: paste.date });
	socket.emit('recv', { id: paste._id, raw_data: paste.data, date: paste.date });
}

export function run(io) {
	io.sockets.on('connection', function(socket) {
		socket.on('new', function (req) {
			pastes.addPaste({ data: req.data, cookie: req.cookie, parent: req.parent }, function(err: Error, objects: any[]) {
				if (err) {
					return console.error(err);
				} else {
					for (var i = 0; i < objects.length; i++) {
						if (objects[i].hasOwnProperty("_id"))
							broadcastPaste(socket, objects[i]);
							socket.emit('new', { id: objects[i]._id });
					}
				}
			});
		});

		socket.on('auth', function(req) {
			return pastes.getPaste(req.id, function(err: Error, doc: any) {
				if (err) return console.error(err);
				if (doc && doc.cookie && doc.cookie === req.cookie) {
					return socket.emit('auth', { auth: true });
				} else {
					return socket.emit('auth', { auth: false });
				}
			});
		});

		socket.on('get', function (req) {
			return pastes.getPaste(req.id, function (err: Error, doc: any) {
				if (err) return console.error(err);
				if (doc) {
					return socket.emit('get', {
						data: marked.parse(doc.data),
						raw_data: doc.data,
						date: doc.date,
						id: doc._id, 
						parent: doc.parent
					});
				} else {
					return socket.emit('get', {});
				}
			});
		});

		socket.on('edit', function (req) {
			if (! req.cookie) return socket.emit('edit', {});

			return pastes.editPaste(req, function (err: Error, doc: any) {
				if (err) return console.error(err);
				if (doc) {
					broadcastPaste(socket, doc);
					return socket.emit('edit', {
						data: marked.parse(doc.data),
						raw_data: doc.data,
						date: doc.date
					});
				} else {
					return socket.emit('edit', {});
				}
			});
		});

		socket.on('mine', function (req) {
			if (! req.cookie) return socket.emit('mine', {});

			return pastes.pasteByCookie(req.cookie, function (err: Error, docs: Array) {
				if (err) return console.error(err);

				docs.forEach(function (doc: any, idx, arr) {
					socket.emit('mine', {
						data: marked.parse(doc.data),
						raw_data: doc.data,
						date: doc.date,
						id: doc._id,
						parent: doc.parent
					});
				});
			});
		});
	});
}
